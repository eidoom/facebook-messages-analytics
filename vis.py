#!/usr/bin/env python3

from pathlib import Path
from json import loads
from re import fullmatch, search
from string import punctuation
from datetime import datetime

from matplotlib.pyplot import subplots, show
from pandas import DataFrame, concat, Series
from wordcloud import WordCloud
from nltk.corpus import stopwords


class Visualiser:
    def __init__(self, folder):
        p = Path(folder)
        dataframes = []
        for f in p.iterdir():
            if fullmatch(f"{folder}/message_\\d+.json", str(f)):
                with open(str(f)) as ff:
                    data = loads(ff.read())["messages"]
                dataframes.append(DataFrame.from_dict(data))
        self.data = concat(dataframes, ignore_index=True, sort=True)
        self.data["datetime"] = [
            datetime.fromtimestamp(t / 1000) for t in self.data["timestamp_ms"].values
        ]
        self.i = 0
        self.sw = stopwords.words("english")

    def new_plot(self, title):
        fig, ax = subplots(num=f"{self.i} {title}")
        self.i += 1
        return fig, ax

    @staticmethod
    def show():
        show()

    @staticmethod
    def save(fig, savepath):
        fig.savefig(savepath, bbox_inches="tight", format="pdf")

    def word_cloud(self, word_freqs):
        fig, ax = self.new_plot("Title")
        fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
        wordcloud = WordCloud(
            background_color="white", scale=10, width=1920, height=1080
        ).fit_words(word_freqs)
        ax.imshow(wordcloud, interpolation="bilinear")
        ax.axis("off")
        return fig

    def words(self, data):
        wrds = []
        for m in data.content.values:
            if type(m) is str:
                for ww in m.split(" "):
                    for w in ww.split("'"):
                        if 2 < len(w) < 34:
                            wd = w.strip(punctuation).lower()
                            if not search("\\W", wd):
                                wrds.append(wd)
        return Series(wrds)

    def word_freq(self, data):
        word_freqs = (
            self.words(data)
            .value_counts()
            .rename_axis("word")
            .reset_index(name="frequency")
        )
        wf = word_freqs[[w not in self.sw for w in word_freqs.word]]
        return Series(wf["frequency"].values, index=wf["word"])

    def get_user(self, user):
        return self.data[self.data["sender_name"] == user]

    def vocabulary(self, user):
        lens = Series([len(w) for w in self.words(self.get_user(user)).values])
        cnt = (
            lens.value_counts(normalize=True)
            .rename_axis("Words of length")
            .reset_index(name="Frequency")
            .sort_values(by="Words of length")
        )
        cnt = cnt[cnt["Words of length"] > 1]
        title = f"Vocabulary of {user}"
        fig, ax = self.new_plot(title)
        ax.set_title(title)
        cnt.plot.bar(x="Words of length", y="Frequency", ax=ax)


if __name__ == "__main__":
    vis = Visualiser("data")

    # vis.vocabulary("Ryan Moodie")

    # fig = vis.word_cloud(vis.word_freq())
    # vis.save(fig, "test.pdf")

    vis.show()
