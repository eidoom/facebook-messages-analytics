# facebook-messages-analytics

## Getting the information

Navigate to https://www.facebook.com/settings?tab=your_facebook_information and choose `Download your information`.
Select desired `Date range`.
Choose `Format: JSON` and `Media quality: Low`.
Select only `Messages` under `Your information`.
Click `Create File`.
Facebook will email you when the file is ready for download.
I found this took a couple of hours.

When the file is ready, download it here, then
```shell
unzip facebook-USERNAME.zip
cp facebook-USERNAME/messages/inbox/FRIEND/* data/
```
where FRIEND is lowercase (the capitalised version has media files).
This copies the message jsons to a folder `data`.

## Dependancies

```shell
sudo dnf install python3-matplotlib python3-pandas python3-nltk
pip install --user wordcloud
python3 -m nltk.downloader popular
```

* [wordcloud](https://amueller.github.io/word_cloud/index.html)
* [nltk](https://www.nltk.org/)
* [pandas](https://pandas.pydata.org/pandas-docs/stable/)
* [matplotlib](https://matplotlib.org/contents.html)
